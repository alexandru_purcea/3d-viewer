import React from 'react';

export default function Lights(props) {
    const { type } = props;
    const Light = type;

    return <Light {...props} />;
}
