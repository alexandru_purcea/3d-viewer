import React, { useRef } from "react";
import { useFrame, useLoader, extend } from "react-three-fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

extend({ OrbitControls });

export default function SceneContainer() {
    const group = useRef();
    const { nodes } = useLoader(GLTFLoader, "models/portrait.glb");

    useFrame(() => {
        group.current.rotation.y += 0.001;
    });

    return (
        <group ref={group}>
            <mesh geometry={nodes.entity_2.geometry}>
                <meshStandardMaterial
                    attach="material"
                    color="white"
                    roughness={5}
                    metalness={0.8}/>
            </mesh>
        </group>
    );
}
