import React, { Suspense } from "react";
import { Canvas } from "react-three-fiber";
import SceneContainer from "./containers/SceneContainer";
import CameraControls from "./components/CameraControls";
import Lights from "./components/Lights";
import "./App.css";

function App() {
  return (
    <Canvas className="canvas" camera={{ position: [0, 0, 15], fov: 2 }}>
        <CameraControls />

        <Lights
            type="AmbientLight"
            color={0xffffff}
            intensity={6}
            position={[0, 0, 0]}/>

        <Lights
            type="PointLight"
            color={0x447b9a}
            intensity={9}
            position={[0, 100, 0]}/>

        <Suspense fallback={<mesh/>}>
            <SceneContainer />
        </Suspense>
    </Canvas>
  );
}

export default App;
